package com.example.restapp.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.restapp.R;
import com.example.restapp.entities.Post;
import com.example.restapp.services.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        enviarPost();
    }

    private void enviarPost() {
        Post post = new Post(1,"Lorem ipsum","Lorem ipsum dolor sit amet..." );
        RetrofitService.getServico().criarPost(post).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                ((TextView)findViewById(R.id.textViewCodigoRetorno)).setText(String.valueOf(response.code()));
                ((TextView)findViewById(R.id.textViewNovoIdCriado)).setText(String.valueOf(response.body().getId()));
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.e(this.getClass().getName(), "onFailure: " + t.getMessage());
            }
        });
    }

}
